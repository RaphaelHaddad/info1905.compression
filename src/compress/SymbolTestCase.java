package compress;
import junit.framework.TestCase;

public class SymbolTestCase extends TestCase{
	Symbol curr;
	Symbol left;
	Symbol right;
	Symbol parent;
	
	public void setUp() throws Exception {
		 curr = new Symbol('b',1);
		 left = new Symbol('l',200);
		 right = new Symbol('r',200);
		 parent = new Symbol('p',200);
		 curr.left = left;
		 curr.right = right;
		 curr.parent = parent;
	}
	
	public void test() throws Exception {
		assertEquals('b', curr.value);
		assertEquals(1,curr.fx);
		assertEquals(curr.left,left);
		assertEquals(curr.right,right);
		assertEquals(curr.parent,parent);
		assertEquals(-1,curr.compareTo(left));
	}

}
