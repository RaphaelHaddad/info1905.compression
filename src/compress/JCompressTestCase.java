package compress;
import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;

public class JCompressTestCase extends TestCase{


	
	public void test() throws Exception{
		compress("JCompress.java","out.zap");//my code
		decompress("out.zap", "JCompress.java.unzaped");
		// diff JCompress.java.unzaped JCompress.java
		/* 
		Raph:assign2 raph$ diff JCompress.java.unzaped JCompress.java
		Raph:assign2 raph$ ls -l
		total 9776
		-rw-r--r--   1 raph  staff     7683 25 Sep 16:55 JCompress.java      --->ACTUAL SIZE<---
		-rw-r--r--   1 raph  staff     7683 25 Sep 17:01 JCompress.java.unzaped
		drwxr-xr-x   4 raph  staff      136 25 Sep 16:15 bin
		-rw-r--r--   1 raph  staff  2486813 25 Sep 16:57 dict
		-rw-r--r--   1 raph  staff  2486813 25 Sep 17:00 dict.unzaped
		drwxr-xr-x  15 raph  staff      510 25 Sep 14:27 doc
		-rw-r--r--   1 raph  staff     4923 25 Sep 17:01 out.zap   --->COMPRESSED SIZE<----
		drwxr-xr-x   4 raph  staff      136 25 Sep 15:59 src
		Raph:assign2 raph$ 

		 */
		
		
		
		compress("dict","out.zap");//usr/dict/words
		decompress("out.zap", "dict.unzaped");
		//diff dict dict.unzaped 
		/*
		 Raph:assign2 raph$ diff dict dict.unzaped
		Raph:assign2 raph$ ls -l
		total 12408
		-rw-r--r--   1 raph  staff     7683 25 Sep 16:55 JCompress.java
		-rw-r--r--   1 raph  staff     7683 25 Sep 17:03 JCompress.java.unzaped
		drwxr-xr-x   4 raph  staff      136 25 Sep 16:15 bin
		-rw-r--r--   1 raph  staff  2486813 25 Sep 16:57 dict ---> ACTUAL SIZE <---
		-rw-r--r--   1 raph  staff  2486813 25 Sep 17:03 dict.unzaped
		drwxr-xr-x  15 raph  staff      510 25 Sep 14:27 doc
		-rw-r--r--   1 raph  staff  1355092 25 Sep 17:03 out.zap --->COMPRESSED SIZE<---
		drwxr-xr-x   4 raph  staff      136 25 Sep 15:59 src
		Raph:assign2 raph$ 

		 */
		
		System.exit(0);
	}
	
	public void compress(String input,String output) throws IOException{
		String[] args = new String[4];
		args[0] ="-c";
		args[1] =input; ///My own Code :D
		args[2] = "-o";
		args[3] = output;
		JCompress.main(args);
	}
	
	public void decompress(String input,String output) throws IOException{
		String[] args = new String[4];
		args[0] ="-x";
		args[1] =input; ///My own Code :D
		args[2] = "-o";
		args[3] = output;
		JCompress.main(args);
	}
	


}
