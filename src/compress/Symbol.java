package compress;
/**
 * 
 * This object acts as both a leaf node or as the whole tree. 
 * @author Raphael Haddad
 *
 */
public class Symbol implements Comparable<Symbol>{
	/** to parent node */
	public Symbol parent = null;
	
	/** reference to left node */
	public Symbol left = null;
	
	/** reference to right node */
	public Symbol right = null;
	
	/** how many times the value occurred */
	public int fx = -1;
	
	/** the value of the symbol */
	public char value;
	
	/** how this node was accessed from it's parent node (either a 1 or 0) */
	public char code;
	
	/** the address to the symbol (how it would be accessed from a tree) */
	public String address = "";
	

	
	
	/**
	 * a constructor that just sets the value and frequency
	 * @param c character (value of the symbol)
	 * @param fx the frequency or how often the symbol occurred
	 */
	public Symbol(char c, int fx) {
		this.value = c;
		this.fx = fx;
	}
	
	/**
	 * a constructor that just sets an integer, usually used when generating a sub-tree
	 * 
	 * @param fx how many times a symbol occurred
	 */
	public Symbol(int fx) {
		this.fx = fx;
	}
	
	/**
	 * a constructor that only sets the left and right nodes
	 * 
	 * @param left symbol or node
	 * @param right symbol or node
	 */
	public Symbol(Symbol left, Symbol right) {
		this.left = left;
		this.right = right;
	}
	
	
	/**
	 * required to sort the list. the two frequencies are compared to sort the list
	 * 
	 * @param s a symbol or something to compare to
	 */
	public int compareTo(Symbol s) {
		if (s.fx < this.fx) {
			return 1;
		} else if (s.fx > this.fx) {
			return -1;
		}
		return 0;
		
	}
	

}
