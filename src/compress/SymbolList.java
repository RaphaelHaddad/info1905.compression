package compress;
import java.util.ArrayList;
/**
 * basically an array list of Symbols with methods that are overloaded.
 * @author raph
 *
 */

public class SymbolList extends ArrayList<Symbol>{
	

/**
 * a character is passed to see if it is in the list or not
 * 
 * @param c the character 
 * @return whether <code>c</code> is in the list
 */
		public boolean contains (char c) {
			for (Symbol s: this) {
				if (s.value == c) {
					return true;
				}
			}
			return false;
		}
		
		/**
		 * a character is passed to then retrieve the actual symbol
		 * 
		 * @param c a primative java character
		 * @return the Symbol with character <code>c</code>
		 */
		public Symbol get (char c) {
			for (Symbol s: this) {
				if (s.value == c) {
					return s;
				}
			}
			return null;
		}
		
		/**
		 * the address is passed to this method then returns which symbol has the address.
		 * 
		 * @param add the address of the leaf in the tree
		 * @return the symbol which has the address <code>add</code>
		 */
		public Symbol get (String add) {
			for (Symbol s: this ){
				if (s.address.equals(add)) {
					return s;
				}
			}
			return null;
		}
		
		/**
		 * a method that adds the character <code>c</code> as a new object
		 * @param c
		 */
		public void add (char c) {
			this.add(new Symbol(c,1));
		}
		
		
		/**
		 * a method that increments the object with character <code>c</code>
		 * @param c
		 */
		public void increment (char c) {
			for (Symbol s : this) {
				if (s.value == c) {
					s.fx += 1;
				}
			}
		}
		
	}


