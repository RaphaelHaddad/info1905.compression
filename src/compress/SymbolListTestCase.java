package compress;
import junit.framework.TestCase;

public class SymbolListTestCase extends TestCase {
	
	Symbol curr;
	SymbolList list;
	
	public void setUp() throws Exception {
		 curr = new Symbol('b',1);
		 list = new SymbolList();
	}
	
	
	public void testadd() throws Exception {
		list.add('b');
		assertEquals(1,list.size());
		list.add('c');
		assertEquals(2,list.size());
		
	}
	
	public void testContains() throws Exception {
		list.add('b');
		assertTrue(list.contains('b'));
		assertFalse(list.contains('e'));
	}
	
	public void testget() throws Exception {
		list.add(curr);
		curr.address = "1001";
		assertEquals(curr, list.get('b'));
		assertEquals(curr, list.get("1001"));
		assertEquals(curr,list.get(0));
	}
	
	public void testIncrement() throws Exception {
		list.add(curr);
		list.increment('b');
		assertEquals(2,curr.fx);
	}

}
