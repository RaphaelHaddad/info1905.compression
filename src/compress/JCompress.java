

package compress;




import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.PriorityQueue;

/**
 * The main class of the Program
 * 
 * @author Raphael Haddad
 *
 */
public class JCompress {



	/**
	 * 
	 * Main program, this is run when the program is executed. This basically determines whether to compress or decompress
	 * @param args
	 */
	public static void main(String[] args) throws IOException{
		//Var
		final String inFile = args[1];
		File file;
		final String outFile = args[3];
		//End Var
		
		
		file = new File(inFile);

		if (!file.exists()) {
			System.out.println("File '"+inFile +"' not Found");
		} else if ( (!args[0].equals("-c") && (!args[0].equals("-x"))) || (!args[2].equals("-o"))) {
			System.out.println("Invalid arguments");
		} else {
			if (args[0].equals("-c")) { ///we need to compress
				compress(inFile,outFile);
			} else if (args[0].equals("-x")) {
				decompress(inFile,outFile);
			} 
		}
		//System.exit(0);
	}
	
/**
 * This method decompresses the file. It first generates the tree (however does not return), but puts each leaf in a list, which stores the actual address
 * of the symbol.
 * 
 * @param input The string of the text file to decompress
 * @param output The file to write the decompression to
 * @throws IOException
 */
	private static void decompress(String input,String output) throws IOException {
		//Var
		SymbolList symbols;
		BitInputStream in;
		BitOutputStream out;
		//End Var
		
		in = new BitInputStream(input);
		out = new BitOutputStream(output);
		
		

		symbols = new SymbolList();
		decodeTree(in,symbols);
		
		for (Symbol s: symbols) { //save the codes to the Symbols
			s.address = getAddress(s,"");
		}
		
		int bcount = in.read(32);//reads the amount of bits	
		String address = "";
		int bit = in.read(1);
		while (bcount > 0) {
			address += Integer.toString(bit); 
			if (symbols.get(address) != null) {
				out.write(8,symbols.get(address).value);
				address = "";
			}
			bit = in.read(1);
			bcount--;
		}
		out.close();
		in.close();
	}
	
	
/**
 * This method compresses the file, firstly it collects
 * all symbols sorts them. Then generates the tree. It 
 * then writes the tree to the text file, then the length of the bits to write and finally the encoded bits
 * 
 * @param input the name of the file that will be compressed
 * @param output the name of the file that will contain the decompressed data
 * @throws IOException
 */
	private static void compress(String input,String output) throws IOException{
		//Var
		SymbolList symbols;
		BufferedReader br;
		int ascii;
		char charac;
		Symbol tree;
		//End Var
		symbols = new SymbolList();
		br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(input))));
		ascii = br.read();
		charac = (char)ascii;
		while (ascii != -1)   {
			if (!symbols.contains(charac)) {
				symbols.add(charac);
			} else {
				symbols.increment(charac);
			}
			ascii = br.read();
			charac = (char)ascii;
		}
		Collections.sort(symbols);
		tree = generateTree(symbols);
		encode(symbols,tree,input,output);

		br.close();
	}

	/**
	 * this method is called from the method <code>Compress</code>. It generates the huffman tree and then
	 * adds the address of each leaf. 
	 * 
	 * @param symbols the list of leafs. The address of these leafs will be written to each Symbol
	 * @return return the root of the tree
	 */
	public static Symbol generateTree(SymbolList symbols) {
		//Var
		PriorityQueue<Symbol> initial;
		Symbol left = null;
		Symbol right = null;
		Symbol newSymbol;
		//End Var;
		initial = new PriorityQueue<Symbol>();
		for (Symbol s : symbols) {
			initial.offer(s);
		}
		while (initial.size() > 1) {
			left = (Symbol)initial.remove();// smaller always one the left zero
			right = (Symbol)initial.remove();
			newSymbol = new Symbol(left.fx + right.fx);
			newSymbol.left = left;
			newSymbol.right = right;
			left.parent = newSymbol;
			right.parent = newSymbol;
			left.code = '0';
			right.code = '1';
			initial.offer(newSymbol);
		}
		for (Symbol s: symbols) { //save the codes to the Symbols
			s.address = getAddress(s,"");
		}

		return (Symbol)initial.remove();
	}


	/**
	 * recursively find the address of each leaf in the tree.
	 * @param s the tree root
	 * @param pos the current position or address, this is initially passed a blank string.
	 * @return the address of the leaf
	 */
	public static String getAddress(Symbol s,String pos){
		if (s.parent != null) {
			return getAddress(s.parent,pos.concat(Character.toString(s.code)));
		} else {
			StringBuffer buffer = new StringBuffer(pos);
			return buffer.reverse().toString();
		}
	}
	
	/**
	 * this method does the encoding for the data. it firstly calculates how many bits are needed to store the data
	 * then writes the bits (so that when they are re-read it is known when to stop). It then writes the encoded data.
	 * 
	 * @param symbols the list of symbols or leafs of the tree
	 * @param tree the tree
	 * @param input the input file
	 * @param output the output file
	 * @throws IOException
	 */
	public static void encode (SymbolList symbols, Symbol tree,String input, String output) throws IOException{
		BitInputStream in = new BitInputStream(input);
		int bcount =0;///this stores how many bits are stored. The text file must be read over twice to determine this	
		int ascii;
		char charac;
		ascii = in.read(8);
		charac = (char)ascii;
		while (ascii != -1)   {
			for (char c: symbols.get(charac).address.toCharArray()) {
				bcount++;
			}
			ascii = in.read();
			charac = (char)ascii;
		}
		
		BitOutputStream out = new BitOutputStream(output);
		writeTree(tree,out);//write tree to file		
		out.write(32,bcount); 	
		in = new BitInputStream(input);
		ascii = in.read(8);
		charac = (char)ascii;
		while (ascii != -1)   {
			for (char c: symbols.get(charac).address.toCharArray()) {
				out.write(1, (int)c -48);
			
			}
			ascii = in.read();
			charac = (char)ascii;
		}
		out.close();
		in.close();
	}
	
/**
 * This method writes the tree recursively to the text file, each sub-tree is denoted by 0 and each leaf is denoted by 1. adapted from this C# code
 * http://stackoverflow.com/questions/759707/efficient-way-of-storing-huffman-tree
 * @param tree the tree root
 * @param out the output textfile
 * @throws IOException
 */
	public static void writeTree(Symbol tree,BitOutputStream out) throws IOException { 
	    if (tree.left == null)
	    {
	    	out.write(1,1);
	    	out.write(8,tree.value);
	    }
	    else
	    {
	    	out.write(1,0);
	    	writeTree(tree.left, out);
	    	writeTree(tree.right, out);
	    }
	}
	
	/**
	 * this method decodes the tree from the text file, each subtree is denoted by 0 and each leaf is denoted by 1. adapted from the
	 * following C# code.
	 * http://stackoverflow.com/questions/759707/efficient-way-of-storing-huffman-tree
	 * 
	 * @param in the input file stream
	 * @param symbols list of all the leafs
	 * @return root of the tree
	 * @throws IOException
	 */
	public static Symbol decodeTree (BitInputStream in, SymbolList symbols) throws IOException{
		Symbol add;
		if (in.read(1) == 1){
			add = new Symbol((char)in.read(8),-2);
			symbols.add(add);
			return add;
		} else{
			Symbol leftChild = decodeTree(in,symbols);
			Symbol rightChild = decodeTree(in,symbols);
			leftChild.code = '0';
			rightChild.code = '1';
			add = new Symbol(leftChild, rightChild);
			leftChild.parent = add;
			rightChild.parent = add;
			return add;
		}

	}

	}


